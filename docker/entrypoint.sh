#!/bin/bash
set -e

PSQL () {
  PGPASSWORD=${POSTGRES_PASSWORD} psql -h ${POSTGRES_HOST} -U ${POSTGRES_USER} ${POSTGRES_DB} "$@"
}

RETRIES=20
until PSQL -c "select 1" > /dev/null 2>&1 || [ $RETRIES -eq 0 ]; do
  echo "Waiting for postgres server."
  sleep 1
done

cd /project
python manage.py migrate
python manage.py runserver 0.0.0.0:8000

