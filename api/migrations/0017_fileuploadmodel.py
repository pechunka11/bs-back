# Generated by Django 3.0.7 on 2020-07-06 10:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0016_auto_20200706_1153'),
    ]

    operations = [
        migrations.CreateModel(
            name='FileUploadModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(blank=True, max_length=255, null=True)),
                ('file', models.ImageField(upload_to='file_upload_model')),
                ('extension', models.CharField(max_length=10)),
            ],
            options={
                'ordering': ('-created',),
                'abstract': False,
            },
        ),
    ]
