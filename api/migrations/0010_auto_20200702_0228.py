# Generated by Django 3.0.7 on 2020-07-01 21:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0009_blog_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='bookmarks',
            field=models.ManyToManyField(null=True, to='api.Bookmark'),
        ),
        migrations.AlterField(
            model_name='blog',
            name='likes',
            field=models.ManyToManyField(null=True, to='api.Like'),
        ),
        migrations.AlterField(
            model_name='blog',
            name='tags',
            field=models.ManyToManyField(null=True, to='api.Tag'),
        ),
        migrations.AlterField(
            model_name='blog',
            name='views',
            field=models.ManyToManyField(null=True, to='api.View'),
        ),
        migrations.AlterField(
            model_name='clinic',
            name='bookmarks',
            field=models.ManyToManyField(null=True, to='api.Bookmark'),
        ),
        migrations.AlterField(
            model_name='clinic',
            name='likes',
            field=models.ManyToManyField(null=True, to='api.Like'),
        ),
        migrations.AlterField(
            model_name='clinic',
            name='views',
            field=models.ManyToManyField(null=True, to='api.View'),
        ),
        migrations.AlterField(
            model_name='clinicservice',
            name='bookmarks',
            field=models.ManyToManyField(null=True, to='api.Bookmark'),
        ),
        migrations.AlterField(
            model_name='clinicservice',
            name='likes',
            field=models.ManyToManyField(null=True, to='api.Like'),
        ),
        migrations.AlterField(
            model_name='clinicservice',
            name='views',
            field=models.ManyToManyField(null=True, to='api.View'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='bookmarks',
            field=models.ManyToManyField(null=True, to='api.Bookmark'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='likes',
            field=models.ManyToManyField(null=True, to='api.Like'),
        ),
        migrations.AlterField(
            model_name='coupon',
            name='views',
            field=models.ManyToManyField(null=True, to='api.View'),
        ),
        migrations.AlterField(
            model_name='faq',
            name='bookmarks',
            field=models.ManyToManyField(null=True, to='api.Bookmark'),
        ),
        migrations.AlterField(
            model_name='faq',
            name='likes',
            field=models.ManyToManyField(null=True, to='api.Like'),
        ),
        migrations.AlterField(
            model_name='faq',
            name='views',
            field=models.ManyToManyField(null=True, to='api.View'),
        ),
        migrations.AlterField(
            model_name='news',
            name='bookmarks',
            field=models.ManyToManyField(null=True, to='api.Bookmark'),
        ),
        migrations.AlterField(
            model_name='news',
            name='likes',
            field=models.ManyToManyField(null=True, to='api.Like'),
        ),
        migrations.AlterField(
            model_name='news',
            name='views',
            field=models.ManyToManyField(null=True, to='api.View'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='bookmarks',
            field=models.ManyToManyField(null=True, to='api.Bookmark'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='likes',
            field=models.ManyToManyField(null=True, to='api.Like'),
        ),
        migrations.AlterField(
            model_name='sale',
            name='views',
            field=models.ManyToManyField(null=True, to='api.View'),
        ),
    ]
