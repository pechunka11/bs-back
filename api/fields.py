from drf_extra_fields.fields import Base64FileField
from rest_framework.exceptions import ValidationError


def check_format_bas64_data_validation(base64_data):
    if base64_data.count(';') != 2:
        raise ValidationError('Not found file type')


class Base64BaseField(Base64FileField):
    ALLOWED_TYPES = ['*']

    def handler_base64_data(self, base64_data):
        check_format_bas64_data_validation(base64_data)
        self.fileExtension = base64_data.split(';')[-1]

    def to_internal_value(self, base64_data):
        self.handler_base64_data(base64_data)
        return super().to_internal_value(base64_data)

    def get_file_extension(self, filename, decoded_file):
        return self.fileExtension


class Base64AnyFileField(Base64BaseField):
    def handler_base64_data(self, base64_data):
        check_format_bas64_data_validation(base64_data)
        self.fileExtension = base64_data.split(';')[-1]
        if self.fileExtension not in self.ALLOWED_TYPES:
            self.ALLOWED_TYPES.append(self.fileExtension)


class Base64VideoFileField(Base64BaseField):
    ALLOWED_TYPES = ['avi', 'mpeg', 'wmv', '3gp', 'flv', 'mov', 'mkv', 'ogg',
                     'asf', 'm2ts', 'm4v', 'mp4', 'mts', 'swf', 'vob', 'webm']


class Base64ImageFileField(Base64BaseField):
    ALLOWED_TYPES = ['jpg', 'jpeg', 'png', 'bmp', 'gif', 'ico']

